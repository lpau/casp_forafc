# revised CASP for AFC toolbox

This is the repository for the implementation of the revised computational auditory signal processing and perception model (CASP) as described in Paulick et al., 2024. Please read the corresponding paper for full details of the model and model stages. The toolbox is specifically designed to be integrated in the AFC toolbox for running psychoacoustic experiments in MATLAB.

## Installation

It is a requirement to first install the AFC software package for mathworks MATLAB (Version 1.40 Build 0, Copyright (c) 1999 - 2014 Stephan Ewert. All rights reserved.) This is available at www.aforcedchoice.com

In the following it is assumed that you are familiar with the AFC toolbox and structures for running experiments with a listener and a model. 

The revised CASP for AFC toolbox contains four folders that need to be added to the AFC toolbox: 

* exp_model: This contains the experiment configuration files that tells the AFC that the 'subject' is a model and sets the experiment dependent model settings
* experiments: This contains all the necessary experiment files for the example tasks (intensity discrimination, forward masking, modulation detection). Here the procedure is defined, experiment is setup and signals are generated. 
* model: This contains all the necessary model files. It has all the preprocessing and backend steps of the model. See the specific files for details.
* toolbox: This contains all the necessary auxiliary files and model stages necessary for running the experiments and model. 


To get started, use git to clone this repository into your computer. Or download and unzip all the included files to your MATLAB directory and add them to your AFC path. 
```
https://gitlab.com/lpau/casp_forafc.git
```

## Getting started

To run an example experiment you can either take a look at 'EXAMPLE_sims.m' or use the command line. Simulations are started with the command
```
afc_main('experiment','CASP','condition','model_config')
```
'Experiment' is the name of the experiment you want to run, 'CASP' specifies the model, 'condition' includes optional conditions for the experiment, and 'model_config' specifies the model configuration (see details below). As an example, if you want to run the modulation detection task for the 31 Hz wide noise carrier, you would run
```
afc_main('modulation','CASP','31','model_config')
```
Here, you must still specify the model_config (for details see exp_model/model_flags) which is determined by two letters. The first letter specifies the range of auditory frequency channels: Either a manually determined narrowband ('N') configuration, an automatic ('A') channel selection, or a broadband ('B') configuration. The second letter specifies whether to use the original ('O') parameter set and model stages from the 2008 CASP model (Jepsen et al, 2008) or the revised ('R') parameter set and model stages (Paulick et al., 2024). I.e., to run the modulation detection task with the full revised model with automatic channel selection: 
```
afc_main('modulation','CASP','31','AR')
```
The simulation results are saved as an ASCII file 'modulation_CASP_31_AR.dat' in the results folder in your AFC toolbox.

## Running your own experiment

To run your own experiment with CASP simply use your experiment files to run the experiment and specify CASP as the subject. Things to note: The input signals to CASP must be in natural units of Pascal, so make sure the levels are set correctly in your experiment_user file. Additionally, you will need an 'experiment_CASP_cfg' file that specifies the experiment dependent model parameters. Here you must specify the supra-threshold template level, how many averages you would like to use for the template generation, and the optional manually determined auditory frequency range. This file must call model_flags. See the example files in the folder 'exp_model' for a template. 

## References

* Ewert, S. D. (2013). “AFC - A modular framework for running psychoacoustic experiments and computational perception models,” in The International Conference on Acoustics AIA-DAGA, Merano, Italy, pp. 1326–1329
* Jepsen, M. L., Ewert, S. D., and Dau, T. (2008). “A computational model of human auditory signal processing and perception,” The Journal of the Acoustical Society of America 124(1), 422–438
* Paulick, L.C., Relaño-Iborra, H., Dau, T. (2024). “The computational auditory signal processing and perception model (CASP): A revised version” bioRxiv 2024.07.31.605576; doi: https://doi.org/10.1101/2024.07.31.605576

For questions or inquiries please contact: lpau@dtu.dk

## Citation

To site this repository use the following: 

* Paulick, L.C., Relaño-Iborra, H., Dau, T. (2024). “The computational auditory signal processing and perception model (CASP): A revised model implementation” Technical University of Denmark. Model. https://doi.org/10.11583/DTU.26413378.v1

This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License [CCBY 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
