% tone-in-noise forward masking experiment as in Jepsen et al. 2008

function forward_jepsen2008_set

global def
global work
global setup

switch work.userpar1 % Masker level
case '40'
   setup.masklvl = 40;
case '60'
   setup.masklvl = 60;
case '80'
   setup.masklvl = 80;
otherwise
   error('Other levels not yet implemented!');
end

% Signal frequency
setup.signalfreq = 4000;

% Compute time-windows
setup.hannlen_masker = round(0.002*def.samplerate);
setup.hannlen_signal = round(0.005*def.samplerate);

ramp_full   = sin(linspace(0, pi, 2*setup.hannlen_masker));
ramp_full   = ramp_full(:)';
ramp_shape  = [ramp_full(1:setup.hannlen_masker), ones(1, 0.200*def.samplerate - length(ramp_full)), ramp_full(setup.hannlen_masker+1:end)];
setup.window_masker = ramp_shape(:);

setup.window_signal = hannfl(0.010*def.samplerate,setup.hannlen_signal,setup.hannlen_signal);

% eof
