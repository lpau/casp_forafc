% intjnd_cfg.m - noise intensity JND - config file
% aimed to compare to data from Miller, 1947

% general measurement procedure
def.expname = 'intjnd';		% name of experiment   
def.measurementProcedure = 'transformedUpDown';	% measurement procedure';	% measurement procedure
def.intervalnum = 3;				    % number of intervals
def.rule = [1 1];				        % [up down]-rule: [1 2] = 1-up 2-down
def.varstep = [2.25 0.75 0.25];		    % [starting stepsize ... minimum stepsize] of the tracking variable
def.steprule = -1;				        % stepsize is changed after each upper (-1) or lower (1) reversal
def.reversalnum = 6;				    % number of reversals in measurement phase
def.repeatnum = 5;				        % number of repeatitions of the experiment

% experimental variable (result of procedure yields dependent variable)
def.startvar = 5; 				        % starting value of the tracking variable
def.expvarunit = 'dB';				    % unit of the tracking variable
def.expvardescription = 'Probe level';	% description of the tracking variable

% limits for experimental variable
def.minvar = 0;				    % minimum value of the tracking variable
def.maxvar = 10;			    % maximum value of the tracking variable
def.terminate = 1;				% terminate execution on min/maxvar hit: 0 = warning, 1 = terminate
def.endstop = 3;				% Allows x nominal levels higher/lower than the limits before terminating (if def.terminate = 1) 

% experimental parameter (independent variable)
def.exppar1 = [20 25 35 45 55 70];
dex.exppar1unit = 'dB SPL';
def.exppar1description = 'Noise level';

% interface, feedback and messages 
def.mouse = 1;					    % enables mouse/touch screen control (1), or disables (0) 
def.markinterval = 1;				% toggles visual interval marking on (1), off(0)
def.feedback = 1;				    % visual feedback after response: 0 = no feedback, 1 = correct/false/measurement phase
def.messages = 'default';			% message configuration file, if 'autoSelect' AFC automatically selects depending on expname and language setting, fallback is 'default'. If 'default' or any arbitrary string, the respectively named _msg file is used.
def.language = 'EN';				% EN = english, DE = german, FR = french, DA = danish

% save paths and save function
def.result_path = './results/';				% where to save results
def.control_path ='./control/';				% where to save control files
def.savefcn = 'default';			        % function which writes results to disk

% samplerate and sound output
def.samplerate = 44100;				        % sampling rate in Hz
def.intervallen = 1.500 * 44100;			% length of each signal-presentation interval in samples (might be overloaded in 'expname_set')
def.pauselen = 0.300 * 44100;				% length of pauses between signal-presentation intervals in samples (might be overloaded in 'expname_set')
def.presiglen = 100;				        % length of signal leading the first presentation interval in samples (might be overloaded in 'expname_set')
def.postsiglen = 100;				        % length of signal following the last presentation interval in samples (might be overloaded in 'expname_set')
def.bits = 16;					            % output bit depth: 8 or 16 see def.externSoundCommand for 32 bits

% computing
def.allowpredict = 0;				% if 1 generate new stimuli during sound output if def.markinterval disabled
def.internSoundCommand = 'sound';
def.deviceID = 0;
def.debug = 0;		% set 1 for debugging (displays all changible variables during measurement)

% eof 