% intensity discrimination for tone user file

function intjnd_tone_user

global def
global work
global set

f_tone = set.signalfreq;
t = 0:1/def.samplerate:((set.Ntone-1)/def.samplerate); 

tone = cos(2*pi*f_tone*t).*set.window; 

tref1 = tone.'/rms(tone) * 20e-6*10^(work.exppar1/20);
tuser = tone.'/rms(tone) * 20e-6*10^((work.exppar1 + work.expvaract)/20);

% Zero-pad to 500 ms
tuser = [tuser' zeros(1,def.intervallen-length(tuser))]';
tref1 = [tref1' zeros(1,def.intervallen-length(tref1))]';

presig=zeros(def.presiglen,2);
postsig=zeros(def.postsiglen,2);
pausesig=zeros(def.pauselen,2);

work.signal=[tuser tuser*0 tref1 tref1*0];    % left only
work.presig=presig;							  % must contain the presignal
work.postsig=postsig;						  % must contain the postsignal
work.pausesig=pausesig;						  % must contain the pausesignal

% eof
