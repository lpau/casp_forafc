% modulationDetection - based on example_user file -

function modulation_user

global def
global work
global set

modsine = sin([0:def.intervallen-1]'*2*pi*work.exppar1/def.samplerate);

if set.sinecar == 1 % sinusoidal carrier
    sine = sin([0:def.intervallen-1]'*2*pi*5000/def.samplerate);
    sine = sine/rms(sine)*set.level;
    tref1 = sine;     
    tref2 = sine;
    tuser = sine.* (1 + (10^(work.expvaract/20) * modsine));
else
    tref1 = real(ifft(scut(fft(randn(def.intervallen,1)),set.lowercutoff, set.uppercutoff,def.samplerate))) ;
    tref2 = real(ifft(scut(fft(randn(def.intervallen,1)),set.lowercutoff, set.uppercutoff,def.samplerate))) ;
    tuser = real(ifft(scut(fft(randn(def.intervallen,1)),set.lowercutoff, set.uppercutoff,def.samplerate))) ;
    
    tuser = tuser.*(1 + (10^(work.expvaract/20) * modsine));    % holds the target signal
    
    if set.bandwidth >= 300
            tuser = real(ifft(scut(fft(tuser),set.lowercutoff,set.uppercutoff,def.samplerate)));
    end

end

% set levels in Pa for CASP
tref1 = tref1/rms(tref1)*set.level; 
tref2 = tref2/rms(tref2)*set.level; 
tuser = tuser/rms(tuser)*set.level; 

tref1 = tref1 .* set.window;
tref2 = tref2 .* set.window;
tuser = tuser .* set.window;

presig = zeros(def.presiglen,2);
postsig = zeros(def.postsiglen,2);
pausesig = zeros(def.pauselen,2);

% make required fields in work
work.signal = [tuser 0*tuser tref1 0*tref1 tref2 0*tref2];	    % left = right (diotic) first two columns holds the test signal (left right)
work.presig = presig;											% must contain the presignal
work.postsig = postsig;											% must contain the postsignal
work.pausesig = pausesig;										% must contain the pausesignal

% eof