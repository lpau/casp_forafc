% tone-in-noise forward masking experiment as in Jepsen et al. 2008

function forward_jepsen2008_user

global def
global work
global setup

% Stimuli, two 'empty' (masker only) and one containing both the masker and the probe
tref1 = real(ifft(scut(fft(randn(def.samplerate*0.200,1)),20,10000,def.samplerate))) .* setup.window_masker;
tref2 = real(ifft(scut(fft(randn(def.samplerate*0.200,1)),20,10000,def.samplerate))) .* setup.window_masker;
tuser = real(ifft(scut(fft(randn(def.samplerate*0.200,1)),20,10000,def.samplerate))) .* setup.window_masker;

% Normalize to RMS and set the level in Pa for the model
tref1 = tref1/rms(tref1)*20e-6*10^(setup.masklvl/20);
tref2 = tref2/rms(tref2)*20e-6*10^(setup.masklvl/20);
tuser = tuser/rms(tuser)*20e-6*10^(setup.masklvl/20);

% Zero-pad to make space for the probe tone
tref1 = [tref1; zeros(round(0.200*def.samplerate),1)];
tref2 = [tref2; zeros(round(0.200*def.samplerate),1)];
tuser = [tuser; zeros(round(0.200*def.samplerate),1)];

% Tonal probe (signal)
time_vector_probe = [0:1:0.010*def.samplerate-1]./def.samplerate;
probe = sin(2*pi*setup.signalfreq*time_vector_probe).* setup.window_signal';

% Normalize and calibrate
probe = probe/rms(probe)* 20e-6*10.^(work.expvaract/20);

% Zero-pad pre&post (position the probe at the right time)
probe = [zeros(1,round(0.001*(work.exppar1+200)*def.samplerate)) probe];
probe = [probe zeros(1,0.400*def.samplerate-length(probe))];

% Add signal to noise
tuser = tuser + probe';

% Zero-pad to 500 ms
tuser = [tuser' zeros(1,def.intervallen-length(tuser))]';
tref1 = [tref1' zeros(1,def.intervallen-length(tref1))]';
tref2 = [tref2' zeros(1,def.intervallen-length(tref2))]';

% pre-, post- and pause signals (all zeros)
presig = zeros(def.presiglen,2);
postsig = zeros(def.postsiglen,2);
pausesig = zeros(def.pauselen,2);

% make required fields in work
work.signal = [tuser tuser*0 tref1 tuser*0 tref2 tuser*0];	% left = right (diotic) first two columns holds the test signal (left right)
work.presig = presig;									% must contain the presignal
work.postsig = postsig;									% must contain the postsignal
work.pausesig = pausesig;								% must contain the pausesignal

% eof