% tone-in-noise forward masking experiment as in Jepsen et al. 2008

% general measurement procedure
def.measurementProcedure = 'transformedUpDown';	% measurement procedure
def.intervalnum = 3;			% number of intervals
def.rule = [1 2];				% [up down]-rule: [1 2] = 1-up 2-down
def.varstep = [8 4 2 1];		% [starting stepsize ... minimum stepsize] of the tracking variable
def.steprule = -1;				% stepsize is changed after each upper (-1) or lower (1) reversal
def.reversalnum = 6;			% number of reversals in measurement phase
def.repeatnum = 5;				% number of repeatitions of the experiment

% experimental variable (result of procedure yields dependent variable)
def.startvar = 80; 				% starting value of the tracking variable
def.expvarunit = 'dB';			% unit of the tracking variable
def.expvardescription = 'Probe level';	% description of the tracking variable

% limits for experimental variable
def.minvar = -10;				% minimum value of the tracking variable
def.maxvar = 95;				% maximum value of the tracking variable
def.terminate = 1;				% terminate execution on min/maxvar hit: 0 = warning, 1 = terminate
def.endstop = 3;				% Allows x nominal levels higher/lower than the limits before terminating (if def.terminate = 1) 

% experimental parameter (independent variable)
def.exppar1 = fliplr([-20 -10 -5 0 5 10 20 40 80 150]);
dex.exppar1unit = 'Hz';
def.exppar1description = 'Probe frequency';

% interface, feedback and messages 
def.mouse = 1;					% enables mouse/touch screen control (1), or disables (0) 
def.markinterval = 1;			% toggles visual interval marking on (1), off(0)
def.feedback = 1;				% visual feedback after response: 0 = no feedback, 1 = correct/false/measurement phase
def.messages = 'default';		% message configuration file, if 'autoSelect' AFC automatically selects depending on expname and language setting, fallback is 'default'. If 'default' or any arbitrary string, the respectively named _msg file is used.
def.language = 'EN';			% EN = english, DE = german, FR = french, DA = danish

% save paths and save function
def.result_path = './results/';		% where to save results
def.control_path ='./control/';		% where to save control files
def.savefcn = 'default';			% function which writes results to disk

% samplerate and sound output
def.samplerate = 44100;				% sampling rate in Hz
def.intervallen = 0.5 * 44100;		% length of each signal-presentation interval in samples (might be overloaded in 'expname_set')
def.pauselen = 0.300 * 44100;		% length of pauses between signal-presentation intervals in samples (might be overloaded in 'expname_set')
def.presiglen = 100;				% length of signal leading the first presentation interval in samples (might be overloaded in 'expname_set')
def.postsiglen = 100;				% length of signal following the last presentation interval in samples (might be overloaded in 'expname_set')
def.bits = 16;					    % output bit depth: 8 or 16 see def.externSoundCommand for 32 bits

% computing
def.allowpredict = 0;				% if 1 generate new stimuli during sound output if def.markinterval disabled
def.internSoundCommand = 'sound';
def.bits = 16;
def.deviceID = 0;

% eof
