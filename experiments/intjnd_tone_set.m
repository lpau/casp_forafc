% intjnd_tone_set.m - intensity JND for tone - set file

function intjnd_tone_set

global def
global set


set.signalfreq = 1000; % Tone frequency

% WINDOWING
ramp_samples = round(0.01*def.samplerate);
ramp_full   = hanning(2*ramp_samples);
ramp_full   = ramp_full(:)';
ramp_asc    = ramp_full(1:ramp_samples);
ramp_desc   = ramp_full(ramp_samples+1:end);

set.Ntone = 0.2*def.samplerate;

n_ones    = set.Ntone - length(ramp_full); 
ramp_shape = [ramp_asc, ones(1, n_ones), ramp_desc];
set.window = ramp_shape(:)';

% eof
