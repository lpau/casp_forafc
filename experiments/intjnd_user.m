% intensity discrimination for broadband noise user file

function intjnd_user

global def
global work
global set

tref1 = real(ifft(scut(fft(randn(def.intervallen,1)),150,7000,def.samplerate))).* set.window;
tref2 = real(ifft(scut(fft(randn(def.intervallen,1)),150,7000,def.samplerate))).* set.window;
tuser = real(ifft(scut(fft(randn(def.intervallen,1)),150,7000,def.samplerate))).* set.window;

% Set levels in Pa for CASP
tref1 = tref1/rms(tref1) * 20e-6* 10^(work.exppar1/20);
tref2 = tref2/rms(tref2) * 20e-6* 10^(work.exppar1/20);
tuser = tuser/rms(tuser) * 20e-6* 10^((work.exppar1 + work.expvaract)/20);

presig=zeros(def.presiglen,2);
postsig=zeros(def.postsiglen,2);
pausesig=zeros(def.pauselen,2);

work.signal=[tuser tuser*0 tref1 tref1*0 tref2 tref2*0];	% left only
work.presig=presig;											% must contain the presignal
work.postsig=postsig;										% must contain the postsignal
work.pausesig=pausesig;										% must contain the pausesignal

% eof
