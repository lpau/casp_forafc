% modulationDetection - Dau97

function modulation_set

global def
global work
global set

% make condition dependent entries in structure set
% modified to make work.condition more error tolerant

switch work.userpar1
    case '3'
    case '31'
    case '314'
    case 'sin'
otherwise
   error('illegal bandwidth')
end

if strcmp(work.userpar1,'sin')
    set.sinecar = 1;
else
    set.sinecar = 0;
    set.bandwidth = str2double(work.userpar1);
    set.lowercutoff = 5000 - set.bandwidth/2;
    set.uppercutoff = 5000 + set.bandwidth/2;
end

set.hannlen = round(0.2*def.samplerate); % 200 ms ramps
set.window = hannfl(def.intervallen,set.hannlen,set.hannlen);
set.level = 20e-6*10^(65/20); % dB SPL

% eof