% intensity discrimination for broadband nosie set file

function intjnd_set

global def
global work
global set

% Window function
set.hannlen=round(0.1*def.samplerate);
set.window = hannfl(def.intervallen,set.hannlen,set.hannlen);

% eof
