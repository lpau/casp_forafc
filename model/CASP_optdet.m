function Y = CASP_optdet(sig,ref,template)
%   Y = CASP_optdet(sig,ref,template)
%                      
% Notes
%   - produces correlation value of the target compared to the masker
%   - 2D correlation values over time and audio frequency

global simwork 
global simdef
global def 

[dim1,dim2,dim3] = size(template);
idx = 1:dim2; 

mutarget = nan(dim3,1);
muref = nan(dim3,1);
    
for mm = 1:dim3

    rule_4th = find(simwork.fc_aud > 4*simwork.fc_mod(mm)); % remove auditory channels below 1/4 of the modulation frequency
    rule_4th = idx(rule_4th);

    if strcmp(simdef.model,'v2008') % original CASP correlates over all three dimensions simulatenously
        ref_av = 0;
        temp_av = 0;
        tar_av = 0;
    elseif strcmp(simdef.model,'v2024') % revised CASP takes into account across frequency differences
        ref_av = mean2(ref(:,rule_4th,mm));
        temp_av = mean2(template(:,rule_4th,mm));
        tar_av = mean2(sig(:,rule_4th,mm));
    end

    muref(mm) = sum(sum((ref(:,rule_4th,mm) - ref_av).*(template(:,rule_4th,mm) - temp_av)));
    mutarget(mm) = sum(sum((sig(:,rule_4th,mm) - tar_av).*(template(:,rule_4th,mm) - temp_av)));
    
end
 
mutarget = sum(mutarget,'omitnan');
muref = sum(muref,'omitnan');
Y = max(0,mutarget - max(0,muref));
Y = Y*(1/(def.samplerate / simdef.resample));
