function CASP_init
%   - CASP initalises the model
%   - stores model parameters that remain the same for each presentation

global def
global work
global simdef
global simwork

if def.interleaved == 1
	error('CASP:interleaved','Model not tested for interleaved runs');
end

if def.debug == 1
   disp([work.vpname '_init']);
end

%% Create the simwork structure

simwork=struct(		...	% structure containing all changible simulation variables 
'mue_ref_stored',[],	...	% vector of stored mue's (for style 'td')
'minlvl',0,		...	    % minimal input to feedback loops   
'signal',[],		...	% work signal
'template',[],		...	% template signal
'reference',[],		...	% reference signal
'aktref',[],		...	% actual reference
'aktsig',[],		...	% actual signal
'Naud',[],	        ...	% number of drnl channels 						
'fc_aud',[],  	    ...	% CF's
'Nmod',[],	        ...	% number of mf channels 
'fc_mod',[],	    ...	% mf's
'mf_low',0,		    ...	% lowest mf 
'mf_up',0		    ...	% highest mf
);   

%% Outer/Middle ear filtering

simwork.b_hp = HeadphoneFilter(def.samplerate);     % calc headphone filtercoeffs
simwork.b_me = middleearfilter(def.samplerate);     % calc middle ear filtercoeffs

%% DRNL filterbank

if simdef.fc_min == simdef.fc_max
         simwork.fc_aud = simdef.fc_min;
else
         fc_aud = erbspace(250,8000,50);
         [~,idx1] = min(abs(fc_aud - simdef.fc_min));
         [~,idx2] = min(abs(fc_aud - simdef.fc_max));
         simwork.fc_aud = fc_aud(idx1:idx2);
end

if simdef.optsel == 1
    simwork.fc_aud = erbspace(250, 8000, 50);
end

simwork.Naud = length(simwork.fc_aud);

%% Adaptation loops

if strcmp(simdef.model,'v2008') % original parameters (2008 CASP)
    simwork.minlvls = 1e-5; 
    simwork.tau = [0.005 0.05 0.129 0.253 0.500];
    simwork.maxL = 10;
elseif strcmp(simdef.model,'v2024')  % revised parameters (2024 CASP)

    load(['adaptation_min.mat']) % freq. specific model parameters
    
    simwork.minlvls = nan(1,simwork.Naud);
    for ii = 1:simwork.Naud
        [~,idx] = min(abs(minlvls(:,1) - simwork.fc_aud(ii)));
        simwork.minlvls(ii) = minlvls(idx,2);
    end
    
    simwork.tau = expspace(0.007,0.5,25,5);
    simwork.maxL = 10;	
end


%% Modulation filterbank

simwork.mf_low = simdef.mf_min;	      % set lowest mf 
simwork.mf_up =  simdef.mf_max; 	  % set highest mf 
[simwork.fc_mod, ~ ] = mfbtd(0,simwork.mf_low,simwork.mf_up,1,def.samplerate);
simwork.fc_mod(1) = 2.5;

simwork.Nmod = size(simwork.fc_mod,2);	% maximum number of modulation filters

%% Backend

simwork.det_lev = simdef.det_lev; % simwork.det_lev has to be set for supra-threshold template level

