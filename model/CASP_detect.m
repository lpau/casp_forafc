function response = CASP_detect
%   - CASP detect method
%   - runs correlation & decision functions
%   - calculates response = 0/1

global def
global work
global simdef
global simwork

if def.debug == 1 
	disp([work.vpname '_detect']);
end

%% RUN DETECTION MODEL
																
if isempty(simwork.template) == 1
	CASP_template;	% Generate template if not existing
end

[simwork.aktsig,~] = eval([work.vpname '_preproc(work.signal(:,1))']);	% Current signal+masker after preprocessing
simwork.aktsig = simwork.aktsig - simwork.reference;				

if def.modelShowEnable > 0  % Show the representation, if the option was enabled
	CASP_show(simwork.aktsig,'aktsig.rep.',1);
    pause
end

mue_ref = zeros(def.intervalnum - 1,1);
for i=1:def.intervalnum - 1
	[simwork.aktref,~] = eval([work.vpname '_preproc(work.signal(:,i*2+1))']);	% Current reference (masker-only) after preprocessing
	simwork.aktref = simwork.aktref - simwork.reference;                   

    if def.modelShowEnable > 1  % Opt. displaying
		CASP_show(simwork.aktref,'aktref.rep.',1);
    end

	mue_ref(i) = CASP_optdet(simwork.aktsig,simwork.aktref,simwork.template);     % Optimal detection
    
 end

mue = min(mue_ref);	% Difference from act. corr. to the bigger ref. corr.

[response,simwork] = CASP_mdecide(mue,def,simdef,simwork); % calculate response

simwork.mueact=mue; % Allocate value to simwork structure
