function response = CASP_main
%   makes Decision (e.g. interval selection) based on signal detection

global work

detect = eval([work.vpname '_detect']);	% if 1 signal is detected

switch detect
case 1
   response = work.position{work.pvind}(end);
case 0
   response = 0;	
end





