function CASP_display
%   - CASP display
%   - display response and probability for each presentation
%   - is called from afc_control if it exists

global work
global simwork

if ( work.correct{work.pvind}(end) == 1	)	
   	r = 'correct';								% correct response
else
   	r = 'false';								% false response
end

% Display step number, prob, response
disp([num2str(work.expvaract) ' dB - ' 'step ' num2str(work.stepnum{work.pvind}(end)) '   p(' num2str(simwork.mueact) ') = ' ...
    num2str(simwork.probact) '   ' r]);


end
