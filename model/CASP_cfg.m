%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          CASP - AFC Integration
%                                 CASP_cfg
%                      
%
%   - specify that subject "CASP" is actually a model
%   - disable the response box and sound output etc.
%   - choose to display trail outputs, templates, signals
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def.modelEnable = 1;
def.afcwinEnable = 0; 
def.soundEnable = 0;
def.modelDisplayEnable = 0;
def.modelShowEnable = 0;

def.showEnable = 0;
def.showrun = 0;

def.showtrial = 0;					% shows trial signal after each presentation (0 == off, 1 == on)
def.showspec = 0;					% shows spectra from target and references after each trial (0 == off, 1 == on)
def.showstimuli = 0;
def.showspec_frange = [100 8000];

def.debug = 0;

% eof
   