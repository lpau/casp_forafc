function [Y,simwork] = CASP_mdecide(mue,def,simdef,simwork)                    
%   - produces response depending on AFC task/signal detection
%   - takes into account m different intervals & up/down rule
%   - includes internal variance in optimal detector

global work

if simdef.in_var <= 0
	error('CASP:mdecide', 'simdef.in_var must be > 0');
end
 
switch def.intervalnum
    case 2
	    prob = 1 - (erfc((((mue / sqrt(simdef.in_var)) * 0.707) - 0) * 0.7071068) / 2);
    case 3
	    prob = 1 - (erfc((((mue / sqrt(simdef.in_var)) * 0.765) - 0.423) * 0.7071068) / 2);
    case 4
 	    prob = 1 - (erfc((((mue / sqrt(simdef.in_var)) * 0.810) - 0.668) * 0.7071068) / 2);
    otherwise
	    error('CASP:mdecide', 'Only 2-, 3- and 4-AFC procedures are implemented');
end

simwork.probact = prob;

switch simdef.crit % Y = 1, signal is detected
    case 'soft'
	    r = rand;	% uniformly distributed random number (like in SI version)
	    if prob > r
		    Y = 1;
	    else
		    Y = 0;
	    end
    case 'hard'
	    if def.rule{work.pvind}(1) == 1 && prob > (1 / (2 .^ (1/def.rule{work.pvind}(2))))
		    Y = 1;
	    else
		    Y = 0;
   	    end
    otherwise
	    error('CASP:mdecide', 'simdef.crit either hard or soft');
end
