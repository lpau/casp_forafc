function [Y,yadap] = CASP_preproc(x)
%   - Model preprocessing
%   - creates internal representation based on input signals
%   - Output signal is Y  = [T x M x N] (time x aud freq x mod freq)

global def
global work
global simdef
global simwork

%% PREPARATION

if def.debug == 1 
   disp([work.vpname '_preproc']);
end

Nresamp = floor(def.intervallen/simdef.resample); 
fsR = def.samplerate / simdef.resample; % sampling freq after resampling
fsC = def.samplerate;                   % orig sampling freq 

%% RUN MODEL

% -------------- Outer/Middle ear filtering -------------- %
inoutsig = filter(simwork.b_hp,1,x); % Outer-ear filterring
inoutsig = filter(simwork.b_me,1,inoutsig); % middle-ear-ear filterring

% -------------- DRNL filter -------------- %
outsig = zeros(length(inoutsig),simwork.Naud); % channels are in colums
for n = 1:simwork.Naud % input has to be of dimensions [1 x N]
    outsig(:,n) = drnl_HI(inoutsig.',simwork.fc_aud(n),fsC, simdef.subject,simdef.model);
end 

% -------------- IHC stage -------------- %
if strcmp(simdef.model,'v2008') 
    y = envextract(outsig,fsC);  % Half-wave rectification & LP
    y = y*10^(50/20);            % Gain to compensate for the Outer/middle ear attenuation
    y = y.^2;                    % Expansion (Auditory nerve) 
elseif strcmp(simdef.model,'v2024')
    y = IHC_model(outsig,fsC);
end
    
% -------------- Adaptation -------------- %
y = adaptloop(y,fsC,simwork.maxL,simwork.minlvls,simwork.tau); 
yadap = y; % save signal after adaptation
       
% optional resampling
if simdef.resample > 1
    y = resample(y,1,simdef.resample);
    y = y(1:Nresamp,:);
end
    
% -------------- Modulation filterbank -------------- %
[inf,y] = mfbtd(y,simwork.mf_low,simwork.mf_up,simdef.mf_lpco,fsR);	% MFB incl 150 LP
inf(1) = 2.5;
Y = mfbtdpp(y,inf,fsR); % MFB post-processing

end % EOF















