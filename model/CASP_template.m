 function CASP_template
%   - Calculates the stored template & reference signal
%   - optional plot

global def
global work
global simdef
global simwork

%% Build reference and template

tic;                                                            % time measurement start
disp('computing template and reference representation ...');    

storeExpvaract = work.expvaract;		% Store the current expvar away
work.expvaract = simwork.det_lev;		% Get the right level for deriving the template

if def.debug == 1                       % Optional display for debugging
	disp([def.expname '_user']);
end

eval([def.expname '_user;']);                                           % Calls user-function of the experiment
[simwork.template,yadap_sig] = eval([work.vpname '_preproc(work.signal(:,1))']);	% CASP preprocessing on the template
[simwork.reference,yadap_ref] = eval([work.vpname '_preproc(work.signal(:,3))']);	% CASP preprocessing on the reference

if def.modelShowEnable > 1  % Optional plot of signal representations          
	CASP_show(work.signal(:,1),'test signal',1);
	CASP_show(work.signal(:,3),'reference signal',1);   
end

% ----------- Looping over number of templates ------------- %

for i = 1:(simdef.templ_num - 1)
	if def.debug == 1
		disp(['template and reference number: ' num2str(i) '/' num2str(simdef.templ_num)]);
		disp([def.expname '_user']);
	end
   
	eval([def.expname '_user;']);   % Calls user-function of the experiment
        
	if def.modelShowEnable > 1      % Optional plot of signal representations
   		CASP_show(work.signal(:,1),'test signal',0);
   		CASP_show(work.signal(:,3),'reference signal',0);   
	end
   
	[signal,sig_adap] = eval([work.vpname '_preproc(work.signal(:,1))']);	% casp-processing
    simwork.template = simwork.template + signal;               % adds actual signals
	yadap_sig = yadap_sig + sig_adap;                            
    [signal,sig_adap] = eval([work.vpname '_preproc(work.signal(:,3))']);	% casp-processing
	simwork.reference = simwork.reference + signal;             % adds actual signals
    yadap_ref = yadap_ref + sig_adap;
end

simwork.template = simwork.template / simdef.templ_num;			% Rescaling
simwork.reference = simwork.reference / simdef.templ_num;		% Reference is built

yadap_supra = yadap_sig/simdef.templ_num; % save outputs at adaptation stage for ROI analysis
yadap_ref = yadap_ref/simdef.templ_num;
yadap_diff = yadap_supra - yadap_ref;

simwork.template = simwork.template - simwork.reference;		% Template is built by subtracting reference


%% REGION OF INTEREST 

if simdef.optsel % optional automatic auditory frequency channel selection
    
    smooth_sig = nan(size(yadap_diff));  % based on the template at the output of the adaptation loops
    for ii = 1:length(simwork.fc_aud)    % smooth signal with moving average analysis
        winL = 25/simwork.fc_aud(ii) * def.samplerate;
        smooth_sig(:,ii) = movmean(yadap_diff(:,ii),winL);
    end
    met = rms(smooth_sig);
 
    [~,Iaud] = max(met); % Find channel with largest output
    aux = find(met >= 0.5*max(met)); % Set criterion
    
    Imin = aux(1); Imax = aux(end); 

    [~,minI] = min(abs(simwork.fc_aud - 1/sqrt(2)*simwork.fc_aud(Iaud)));
    Imin = min(minI,Imin);

    [~,maxI] = min(abs(simwork.fc_aud - sqrt(4)*simwork.fc_aud(Iaud)));
    Imax = max(maxI,Imax);

    simwork.roi = Imin:Imax;

    % select channels in relevant structures
    simwork.fc_aud = simwork.fc_aud(simwork.roi); simwork.Naud = length(simwork.fc_aud);

    simwork.template = simwork.template(:,simwork.roi,:);
    simwork.reference = simwork.reference(:,simwork.roi,:);
    if length(simwork.minlvls) > 1
        simwork.minlvls = simwork.minlvls(simwork.roi);
    end
end

%% NORMALISATION

simwork.template_true = nan(size(simwork.template)); 
for ff = 1:simwork.Naud
        % Find bands for fcmod > 0.25 fcaud
        mod_freqs = simwork.fc_mod(find(simwork.fc_mod <= 0.25* simwork.fc_aud(ff)));
        Nmods= length(mod_freqs);
        simwork.template_true(:,ff,1:Nmods) = simwork.template(:,ff,1:Nmods);
 end
fsR = def.samplerate/simdef.resample;
c = sqrt(fsR/sum(simwork.template_true.^2,'all','omitnan'));
simwork.template = c*simwork.template;

%% FINAL stuff

if def.modelShowEnable > 0  % Optional plot
	CASP_show(simwork.template,'template rep.',1);
   	CASP_show(simwork.reference,'reference rep.',1);
end

% restore work.expvaract to experiment_cfg startvar
work.expvaract = storeExpvaract;

eval([def.expname '_user;']);

disp('... finished');
toc  

% DONE
