function CASP_show(in,tit,newfig)
%   - Function for plotting signals
%   - creates 3D plots for multiple channels & modulations
%   - Depending on dimension of the input signal, different plots are shown
%       column-vector: 	time signal -> plot
%       2-d matrix:		time-frequency signal -> mesh
%       3-d matrix:		time-frequency-mf signal -> a whole number of meshs
%
%       tit:	appears on top of the figure
%       newfig:	plot appears in a new (newfig=1) or in the last(newfig=0)

if newfig ~= 0
   	figure;
else
   	H = gcf;
   	figure(H);
end

[LX1,LX2,LX3] = size(in);

if LX3 > 1
	in_MAX = max(max(max(in)));
	in_MIN = min(min(min(in)));
	b = ceil(sqrt(LX3));
	a = ceil(LX3 / b);
    for i=1:LX3 
   		if LX2 > 1   
			subplot(a,b,i),mesh(in(:,:,i)');
			ax=axis;
			axis([ax(1:4) in_MIN in_MAX]);
			title([tit num2str(i)]);
      		else
			subplot(a,b,i),plot(in(:,1,i));
			ax=axis;
			axis([ax(1:2) in_MIN in_MAX]);
			title([tit num2str(i)]);
		end
	end
elseif LX2 > 1
	mesh(in');
	title(tit);
else
	plot(in);
	title(tit);
end

pause(.1);
