%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                      CASP MODEL - AFC Integration
%
%
%  Notes
%   - example script for running AFC experiments with revised CASP
%   - specify which experiments to run
%   - the AFC toolbox must first be installed and afc_addpath run to add all the relevant folders
%   - results are stored in the results folder of AFC
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc

%% SET PATHS

% add pathways
addpath([fileparts(which(mfilename))])
addpath([fileparts(which(mfilename)), filesep, 'exp_model'])
addpath([fileparts(which(mfilename)), filesep, 'experiments'])
addpath([fileparts(which(mfilename)), filesep, 'model'])
addpath([fileparts(which(mfilename)), filesep, 'toolbox'])

% make sure your current directory is the main afc toolbox folder before 
% beginning the simulations otherwise the control and results files are not save in the correct folderes
cd(fileparts(which('afc_addpath'))) 

%% Toggles

do_intjnd = 1;
do_FM = 0;
do_modulation = 0;

%% RUN Intensity Discrimination

if do_intjnd
    afc_main('intjnd_tone','CASP','AR') % tone intjnd
    afc_main('intjnd','CASP','AR')      % noise intjnd

    % Load results
    filenoise = ['results/intjnd_CASP_AR.dat'];
    data.noise = allmean(datread(filenoise));
    filetone = ['results/intjnd_tone_CASP_AR.dat'];
    data.tone = allmean(datread(filetone));
end

%% RUN Forward Masking

if do_FM
    afc_main('forward_jepsen2008','CASP','40','AR') % 40 dB Masker
    afc_main('forward_jepsen2008','CASP','60','AR') % 60 dB Masker
    afc_main('forward_jepsen2008','CASP','80','AR') % 80 dB Masker

    % Load results
    file40 = ['results/forward_jepsen2008_CASP_40_AR.dat'];
    data.noise40 = allmean(datread(file40));
    file60 = ['results/forward_jepsen2008_CASP_60_AR.dat'];
    data.noise60 = allmean(datread(file60));
    file80 = ['results/forward_jepsen2008_CASP_80_AR.dat'];
    data.noise80 = allmean(datread(file80));
end

%% RUN Modulation Detection

if do_modulation
    afc_main('modulation','CASP','3','AR')   % 3   Hz wide carrier
    afc_main('modulation','CASP','31','AR')  % 31  Hz wide carrier
    afc_main('modulation','CASP','314','AR') % 314 Hz wide carrier

    % Load results
    tmtf3 = ['results/modulation_CASP_3_AR.dat'];
    tmtf31 = ['results/modulation_CASP_31_AR.dat'];
    tmtf314 = ['results/modulation_CASP_314_AR.dat'];
    data.tmtf3 = allmean(datread(tmtf3));
    data.tmtf31 = allmean(datread(tmtf31));
    data.tmtf314 = allmean(datread(tmtf314));
end
    