function b = middleearfilter(fs,order)
%MIDDLEEARFILTER   Middle ear filter.
%   Usage: b=middleearfilter(fs,order);
%          b=middleearfilter(fs);
%
%   MIDDLEEARFILTER(fs,order) computes the filter coefficients of a FIR
%   filter or order order approximating the effect of the middle ear.
%
%   MIDDLEEARFILTER(fs) does the same with a FIR filter of order 512.
%  
%   REFERENCES:
%     R. Goode, M. Killion, K. Nakamura, and S. Nishihara. New knowledge
%     about the function of the human middle ear: development of an improved
%     analog model. The American journal of otology, 15(2):145-154, 1994.
%     
%     E. Lopez-Poveda and R. Meddis. A human nonlinear cochlear filterbank.
%     The Journal of the Acoustical Society of America, 110:3107, 2001.
%  
%   #Author: Peter L. Søndergaard 
%   #Author: Katharina Egger 
%   #Author: Alejandro Osses (2021)

% This file is licensed unter the GNU General Public License (GPL) either 
% version 3 of the license, or any later version as published by the Free Software 
% Foundation. Details of the GPLv3 can be found in the AMT directory "licences" and 
% at <https://www.gnu.org/licenses/gpl-3.0.html>. 
% You can redistribute this file and/or modify it under the terms of the GPLv3. 
% This file is distributed without any warranty; without even the implied warranty 
% of merchantability or fitness for a particular purpose. 

if nargin==1
  order = 512;    % desired FIR filter order
end

% data read from Poveda Fig.2, excl. extrapolated points
data = [400	4.728E-09; ...
600	7.577E-09; ...
800	1.000E-08; ...
1000 8.235E-09; ...
1200 6.240E-09; ...
1400 5.585E-09; ...
1600 5.000E-09; ...
1800 4.232E-09; ...
2000 3.787E-09; ...
2200 3.000E-09; ...
2400 2.715E-09; ...
2600 2.498E-09; ...
2800 2.174E-09; ...
3000 1.893E-09; ...
3500 1.742E-09; ...
4000 1.516E-09; ...
4500 1.117E-09; ...
5000 1.320E-09; ...
5500 1.214E-09; ...
6000 9.726E-10; ...
6500 9.460E-10];

% data read from Poveda Fig.2, extrapolated points
extrp = [100 1.181E-09; ...
200	2.363E-09; ...
7000 8.705E-10; ...
7500 8.000E-10; ...
8000 7.577E-10; ...
8500 7.168E-10; ...
9000 6.781E-10; ...
9500 6.240E-10; ...
10000 6.000E-10];

data = [extrp(extrp(:,1) < data(1,1),:); data; extrp(extrp(:,1) > data(end,1),:)];

if fs<=20000
  % In this case, we need to cut the table because the sampling
  % frequency is too low to accomodate the full range.
  indx=find(data(:,1)<fs/2);
  data = data(1:indx(end),:);
else
  % otherwise the table will be extrapolated towards fs/2
  % data point added every 1000Hz
  lgth = size(data,1);
  for ii = 1:floor((fs/2-data(end,1))/1000)
    data(lgth+ii,1) = data(lgth+ii-1,1) + 1000;
    % 1.1 corresponds to the decay of the last amplitude values = approx. ratio
    % between amplitudes of frequency values seperated by 1000Hz
    data(lgth+ii,2) = data(lgth+ii-1,2) / 1.1; 
  end
end 

% for the function fir2 the last data point has to be at fs/2
lgth = size(data,1);
if data(lgth,1) ~= fs/2
  data(lgth+1,1) = fs/2;
  data(lgth+1,2) = data(lgth,2) / (1+(fs/2-data(lgth,1))*0.1/1000);
end

% Extract the frequencies and amplitudes, and put them in the format
% that fir2 likes.
freq=[0;data(:,1).*(2/fs)];
ampl=[0; data(:,2)];

b = fir2(order,freq,ampl);
b = b / 20e-6;      % scaling for SPL in dB re 20uPa
    


