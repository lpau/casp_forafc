function [out,y_lin,y_nlin] = drnl_HI(x,CF,fs,subj,model)
%drnl_HI  Implementation of the dual resonance nonliner (DRNL) filterbank.
%   Usage: outsig = drnl_HI(x,CF,fs,subj,model);
%
%   Input:
%       x     - input signal
%       CF    - centre frequency of filter
%       fs    - sampling frequency
%       subj  - subject, e.g. 'NH' for normal-hearing filterbank
%       model - specify model version, eg. 'v2008' or 'v2024' for
%               implementation from Jepsen et al., 2008 or Paulick et al., 2024
% 
%   Output:
%       out    - output signal of filter centred at CF
%       y_lin  - optional output from the linear pathway
%       y_nlin - optional output from the non-linear pathway
%
%   References: Lopez-Poveda, E. A., and Meddis, R. (2001). 'A human nonlinear cochlear filterbank'
%                   The Journal of the Acoustical Society of America 110(6), 3107–3118
%               Jepsen, M. L., Ewert, S. D., and Dau, T. (2008). 'A computational model of human auditory 
%                   signal processing and perception,' The Journal of the Acoustical Society of America 124(1), 422–43
%
%   Authors: Morten Løve Jepsen (11-2005), Helia Relaño-Iborra (12-2018),
%          Last edited: Lily Cassandra Paulick (07-2024)

[linDRNLpar,nlinDRNLpar] = getDRNLparam(CF,subj,model);

[GTlin_b,GTlin_a] = gammatone(linDRNLpar(1).vals,fs,linDRNLpar(2).vals,linDRNLpar(3).vals/audfiltbw(linDRNLpar(1).vals)); %get GT filter coeffs
[LPlin_b,LPlin_a] = butter(2,linDRNLpar(5).vals/(fs/2)); % get LP filter coeffs

[GTnlin_b,GTnlin_a] = gammatone(nlinDRNLpar(1).vals,fs,nlinDRNLpar(2).vals,nlinDRNLpar(3).vals/audfiltbw(nlinDRNLpar(1).vals)); %get GT filter coeffs
[LPnlin_b,LPnlin_a] = butter(2,nlinDRNLpar(7).vals/(fs/2)); % get LP filter coeffs

% --------------- Linear part ------------- %
y_lin = x.*linDRNLpar(4).vals; % Apply linear gain

% Gammatone filtering
y_lin = filter(GTlin_b,GTlin_a,y_lin);   

for n = 1:linDRNLpar(6).vals % cascade of lowpass filters
    y_lin = filter(LPlin_b,LPlin_a,y_lin);       
end
% --------- End of Linear part ------------ %

% ----------- Non-Linear part ------------- %
y_nlin = x;

% GT filtering before
y_nlin = filter(GTnlin_b,GTnlin_a,y_nlin);

% Broken stick nonlinearity
a = nlinDRNLpar(4).vals;
b = nlinDRNLpar(5).vals;
c = nlinDRNLpar(6).vals;

% pause
y_decide = [a*abs(y_nlin); b*(abs(y_nlin)).^c];
y_nlin = sign(y_nlin).* min(y_decide);

% GT filtering after
y_nlin = filter(GTnlin_b,GTnlin_a,y_nlin);

% then LP filtering
for n = 1:nlinDRNLpar(8).vals % cascade of lowpass filters
    y_nlin = filter(LPnlin_b,LPnlin_a,y_nlin);           
end
% --------- End of Non-Linear part --------- %

out = (y_lin + y_nlin); % add linear and non-linear pathways


