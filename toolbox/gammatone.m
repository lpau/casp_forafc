function [b,a]=gammatone(fc,fs,n,betamul)
%GAMMATONE  Gammatone filter coefficients
%   Usage: [b,a] = gammatone(fc,fs,n,betamul);
%
%   Input parameters:
%      fc    : center frequency in Hz.
%      fs    : sampling rate in Hz.
%      n     :  filter order.
%      beta  :  bandwidth of the filter.
%
%   Output parameters:
%      b     :  nominator coefficients.
%      a     :  denominator coefficients.
%
%   gammatone(fc,fs,n,betamul) computes the filter coefficients of a
%   digital gammatone filter with center frequency *fc*, order *n*, sampling
%   rate *fs* and bandwith determined by *betamul*. The bandwidth *beta* of
%   each filter is determined as *betamul* times `audfiltbw` of the center
%   frequency of corresponding filter.
%
%   #Author : Stephan Ewert
%   #Author : Peter L. Soendergaard (2011)
%   #Author : Christian Klemenschitz

% This file is licensed unter the GNU General Public License (GPL) either 
% version 3 of the license, or any later version as published by the Free Software 
% Foundation. Details of the GPLv3 can be found in the AMT directory "licences" and 
% at <https://www.gnu.org/licenses/gpl-3.0.html>. 
% You can redistribute this file and/or modify it under the terms of the GPLv3. 
% This file is distributed without any warranty; without even the implied warranty 
% of merchantability or fitness for a particular purpose. 

% ------ Checking of input parameters ---------
if ~isnumeric(n) || ~isscalar(n) || n<=0 || fix(n)~=n
  error('%s: n must be a positive, integer scalar.',upper(mfilename));
end

if ~isnumeric(betamul) || ~isscalar(betamul) || betamul<=0
error('%s: beta must be a positive scalar.',upper(mfilename));
end

% ------ Computation --------------------------

% ourbeta is used in order not to mask the beta function.  
ourbeta = betamul*audfiltbw(fc);

nchannels = length(fc);
    
b=zeros(nchannels,n+1);
a=zeros(nchannels,2*n+1);
        
% This is when the function peaks.
delay = 3./(2*pi*ourbeta);

for ii = 1:nchannels      
  % convert to radians
  theta = 2*pi*fc(ii)/fs;
  phi   = 2*pi*ourbeta(ii)/fs;

  alpha = -exp(-phi)*cos(theta);
  
  b1 = 2*alpha;
  b2 = exp(-2*phi);
  a0 = abs( (1+b1*cos(theta)-1i*b1*sin(theta)+b2*cos(2*theta)-1i*b2*sin(2*theta)) / (1+alpha*cos(theta)-1i*alpha*sin(theta))  );
  
  % Compute the position of the pole
  atilde = exp(-phi-1i*theta);
  
  % Repeat the conjugate pair n times, and expand the polynomial
  a2 = poly([atilde*ones(1,n),conj(atilde)*ones(1,n)]);
  
  % Compute the position of the zero, just the real value of the pole
  btilde = real(atilde);
  
  % Repeat the zero n times, and expand the polynomial
  b2 = poly(btilde*ones(1,n));
 
  % Scale to get 0 dB attenuation
  b2=b2*(a0^n);
 
  % Place the result (a row vector) in the output matrices.
  b(ii,:)=b2;
  a(ii,:)=a2;
  
end
    
% Octave produces small imaginary values
b=real(b);
a=real(a);
  
end

  



