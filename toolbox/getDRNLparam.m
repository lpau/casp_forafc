function [linDRNLstruct,NlinDRNLstruct] = getDRNLparam(CF,subj,model)
% Script for getting the (DRNL) filter parameters (Lopez-Poveda, Meddis 2001)
% Authors: Morten Løve Jepsen, 2007, Helia Relaño-Iborra, 2019
%         Last edited: Lily Cassandra Paulick, 2024
%
% usage:  [linDRNLstruct,nlinDRNLstruct] = getDRNLparam(CF,subj,model
%
% Inputs:
%       CF    - centre frequency of filter
%       subj  - subject, e.g. 'NH' for normal-hearing filterbank
%       model - specify model version, eg. 'v2008' or 'v2024' for
%               implementation from Jepsen et al., 2008 or Paulick et al., 2024;
%
% Outputs:
%       linDRNLstruct  - strucure containing the parameter names and values
%           for the linear pathway
%       nlinDRNLstruct - strucure containing the parameter names and values
%           for the non-linear pathway
%
%   References: Lopez-Poveda, E. A., and Meddis, R. (2001). 'A human nonlinear cochlear filterbank'
%                   The Journal of the Acoustical Society of America 110(6), 3107–3118 (table II, and table III)
%               Jepsen, M. L., Ewert, S. D., and Dau, T. (2008). 'A computational model of human auditory 
%                   signal processing and perception,' The Journal of the Acoustical Society of America 124(1), 422–43 The parameters are based on the article "A human nonlinear cochlear


% Initialize the parameter structures
linDRNLstruct  = struct('parname',{},'vals',{}); % initialize linear paramater vector
NlinDRNLstruct = struct('parname',{},'vals',{}); % initialize nonlinear paramater vector

linDRNLstruct(1).parname = 'CF_lin';  linDRNLstruct(2).parname = 'nGTfilt_lin';
linDRNLstruct(3).parname = 'BW_lin';  linDRNLstruct(4).parname = 'g';
linDRNLstruct(5).parname = 'LP_lin_cutoff';  linDRNLstruct(6).parname = 'nLPfilt_lin';

NlinDRNLstruct(1).parname = 'CF_nlin';  NlinDRNLstruct(2).parname = 'nGTfilt_nlin';
NlinDRNLstruct(3).parname = 'BW_nlin';  NlinDRNLstruct(4).parname = 'a';
NlinDRNLstruct(5).parname = 'b';  NlinDRNLstruct(6).parname = 'c';
NlinDRNLstruct(7).parname = 'LP_nlin_cutoff';  NlinDRNLstruct(8).parname = 'nLPfilt_nlin';

% Common parameters not subject to immediate change by HI
if strcmp(model,'v2008')
    linDRNLstruct(2).vals = 3;  % number of cascaded gammatone filters  % for sCASP (Helia) 2
    linDRNLstruct(6).vals = 4;  % no. of cascaded LP filters, % for sCASP (Helia) 4
    NlinDRNLstruct(2).vals = 3; % number of cascaded gammatone filters  % for sCASP (Helia) 2
    NlinDRNLstruct(8).vals = 3; % no. of cascaded LP filters in nlin path, % for sCASP (Helia) 1
elseif strcmp(model,'v2024')
    linDRNLstruct(2).vals = 2;  % number of cascaded gammatone filters  % for sCASP (Helia) 2
    linDRNLstruct(6).vals = 4;  % no. of cascaded LP filters, % for sCASP (Helia) 4
    NlinDRNLstruct(2).vals = 2; % number of cascaded gammatone filters  % for sCASP (Helia) 2
    NlinDRNLstruct(8).vals = 1; % no. of cascaded LP filters in nlin path, % for sCASP (Helia) 1
end

% CASP parameters
linDRNLstruct(1).vals = 10^(-0.06762+1.01*log10(CF));     % Hz, CF_lin,
linDRNLstruct(3).vals = 10^(.03728+.75*log10(CF));        % Hz, BW_lin.
linDRNLstruct(5).vals = 10^(-0.06762+1.017*log10(CF));    % Hz, LP_lin cutoff
NlinDRNLstruct(1).vals = 10^(-0.05252+1.01650*log10(CF)); % Hz, CF_nlin
NlinDRNLstruct(3).vals = 10^(-0.03193+.77*log10(CF));     % Hz, BW_nlin
NlinDRNLstruct(7).vals = 10^(-0.05252+1.01650*log10(CF)); % LP_nlincutoff

%  NH PARAMETERS
if strcmp(subj,'NH') % Model for normal-hearing CASP
    linDRNLstruct(4).vals = 10^(4.20405 -.47909*log10(CF)); % g
    if CF<=1500
        NlinDRNLstruct(4).vals = 10^(1.40298+.81916*log10(CF)); % a,
        NlinDRNLstruct(5).vals = 10^(1.61912-.81867*log10(CF)); % b [(m/s)^(1-c)]
    else
        NlinDRNLstruct(4).vals = 10^(1.40298+.81916*log10(1500)); % a,
        NlinDRNLstruct(5).vals = 10^(1.61912-.81867*log10(1500)); % b [(m/s)^(1-c)]
    end
    NlinDRNLstruct(6).vals = 10^(-.60206); % c, compression coeff       
end

%% EOF