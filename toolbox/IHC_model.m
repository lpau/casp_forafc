function [V] = IHC_model(outsig,fs)
% [V] = IHC_model(outsig,fs)
%       Obtains the output of the inner hair cell transduction stage. This
%       is obtained via three main stages: 
%            1. gain applied to BM velocity 
%            2. physiologically fitted Boltzmann function to convert to Conductance 
%            3. electrical circuit analogy to obtain receptor potential
%
%   Input:
%       outsig - Output of the OHC stage
%         fs   - sampling frequency
%
%   Output:
%       V - Receptor Potential of the inner hair cells [ModelUnits]
%
%   References: Sumner, C. J., Lopez-Poveda, E. A., O'Mard, L. P., and Meddis, R. (2002). 'A revised
%                model of the inner-hair cell and auditory-nerve complex,' 
%                The Journal of the Acoustical Society of America 111(5)
%               Verhulst, S., Altoè, A., and Vasilkov, V. (2018). 'Computational modeling of the human 
%                   auditory periphery: Auditory-nerve responses, evoked potentials and hearing loss,'
%                   Hearing Research 360, 55-75

N = size(outsig,1); %length of signal
Ts = 1/fs;
nchannels = size(outsig,2); 

ster_disp = db2mag(-105)*outsig; % scale input/BM velocity

% met channel
EP = 90E-3;
Cm = 12.5e-12;

% fast/slow K+
Gkf = 19.8e-9;
Gks = 19.8e-9;
Ekf = -71e-3;
Eks = -78e-3;

% MET CHANNEL
Gmet_max = 30e-9; x0 = 20e-9; s0 = 16e-9; s1 = 35e-9;
factor1 = exp((x0-ster_disp)/s0);
factor0 = exp((x0-ster_disp)/s1);
G = Gmet_max./(1+factor0.*(1+factor1));

% Intracellular Potential
V_rest = -0.05703; 
V_now = V_rest;

%pre charge 
zero_sample = round(fs*50e-3);
dummy = zeros(zero_sample,1) + 3.3514e-09;
for i = 1:zero_sample
        Imet = dummy(i,:).*(V_now-EP);
        Ik = Gkf*(V_now-Ekf);
        Is = Gks*(V_now-Eks);
        V_now = V_now - squeeze((Imet + Ik + Is))*Ts/Cm;         
end
V_precharg = V_now;

V = zeros(N,nchannels);
    for ii=1:N
        Imet = -G(ii,:).*(V_now-EP);
        Ik = -Gkf*(V_now-Ekf);
        Is = -Gks*(V_now-Eks);
            V_now = V_now + (Imet + Ik + Is)*Ts/Cm;
            V(ii,:)=V_now;
    end

V = V - V_precharg;

end

