%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          CASP - AFC Integration
%                       forward_jepsen2008_CASP_cfg
%                      
%
%   - experiment model configuration file
%   - outputs a the structure 'simdef' containing the complete
%     model configuration for the experiment
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% model variables
simdef.templ_num = 15;			% number of int. rep. used for deriving the template
simdef.subject = 'NH';          % Subject init, eg, 'NH, 'HIx' or HI2

% implementation stuff
simdef.resample = 4;		    % factor for resampling (i.e. fsIR) either 1,2,4,8,... ( > 1 requires MATLAB-function 'resample' from MATHWORK'S Signal processing toolbox)

% detector variabes
simdef.crit = 'hard';			% detection criterium either 'hard' or 'sof9

switch(varargin{1,1})
    case '40'
        simdef.det_lev = 50;    % supra-threshold template level
    case '60'
        simdef.det_lev = 70;
    case '80'
        simdef.det_lev = 90;
    otherwise
        error('forward_jepsen2008_CASP_cfg: illegal level')
end

% Modulation filterbank parameters
simdef.mf_min = 0;			    % lowest modulation filter
simdef.mf_max = 1000;			% upper most modulation filter (mj init 1000)
simdef.mf_lpco = 1;			    % lowpass cut off of the modulation filter (only 'mfbtd') 1 = 2.5 Hz, 2 = 7.5 Hz

% automatized model configuration
model = varargin{1,end};
fmin = 3600; fmax = 5000; %if using manually defined DRNL channels
model_flags(model,fmin,fmax) 

% eof
