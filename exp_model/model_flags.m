function model_flags(model,fmin,fmax)
% model_flags.m
%     script where flag for experiments is specified which
%     is forwarded in afc main call (last parameter); allows automatized
%     variation of the model configurations 
%     
%     structure: 
%       flag (string) by default composed of
%         flag(1)       : range of DRNL filterbank
%         flag(2)       : specify model, e.g. 2008 or 2024
%     optional: 
%       - flag can be extended arbitrarily 
%     example:
%       'NO' - original 2008 model with manual filterbank selection
%       'AR' - revised 2024 model with automatic filterbank selection
% 

global simdef 

if nargin < 2 && strcmp(model(1),'N')
    error('%s: When choosing the manual narrowband frequency range, you must specify the frequency range as input arguments.',upper(mfilename));
end

%% DRNL Filterbank

switch model(1)
    case 'N' % manual narrowband
        simdef.optsel = 0;
        simdef.fc_min = fmin;
        simdef.fc_max = fmax; 
    case 'B' % broadband
        simdef.optsel = 0;
        simdef.fc_min = 250;
        simdef.fc_max = 8000;
    case 'A' % automatic channel selection
        simdef.optsel = 1;
        simdef.fc_min = 250;
        simdef.fc_max = 8000;
    otherwise
        error('illegal flag for range of BM filterbank')
end

%% Model Configuration

switch model(2)
    case 'O' % CASP original/2008
        simdef.model = 'v2008';
    case 'R' % CASP revised/2024
        simdef.model = 'v2024';
    otherwise
        error('illegal flag for model version')
end

%% Internal Noise Variance

 if strcmp(simdef.model,'v2008')
        simdef.in_var = 130;
 elseif strcmp(simdef.model,'v2024')
        simdef.in_var = 40; 
 end

%eof
