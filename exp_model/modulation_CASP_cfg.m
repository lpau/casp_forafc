%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          CASP - AFC Integration
%                           modulation_CASP_cfg
%                      
%
%   - experiment model configuration file
%   - outputs a the structure 'simdef' containing the complete
%     model configuration for the experiment
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% model variables
simdef.templ_num = 15;			% number of int. rep. used for deriving the template
simdef.subject = 'NH';          % Subject init, eg, 'NH, 'HIx' or HI2

% implementation stuff
simdef.resample = 4;			% factor for resampling (i.e. fsIR) either 1,2,4,8,... ( > 1 requires MATLAB-function 'resample' from MATHWORK'S Signal processing toolbox)

% detector variabes
simdef.crit = 'hard';			% detection criterium either 'hard' or 'soft'
simdef.det_lev = -6;			    % detector level

% Modulation filterbank parameters
simdef.mf_min = 0;			    % lowest modulation filter
simdef.mf_max = 1500;			% upper most modulation filter (mj init 1000)
simdef.mf_lpco = 1;			    % lowpass cut off of the modulation filter (only 'mfbtd') 1 = 2.5 Hz, 2 = 7.5 Hz

% automatized model configuration
model = varargin{1,end};
fmin = 4500; fmax = 8000;  % if manual auditory channel selection
model_flags(model,fmin,fmax);

% eof
